package kucf.solver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileFinder {

	public static List<File> findFiles(String base) {
		List<File> out = new ArrayList<>();
		File root = new File(base);
		dfs(root, out);
		return out;
	}

	private static void dfs(File file, List<File> out) {
		if (file.isFile()) {
			out.add(file);
		} else if (file.isDirectory()) {
			File[] filesInFolder = file.listFiles();
			for (File insideFile : filesInFolder) {
				dfs(insideFile, out);
			}
		}
	}

}
