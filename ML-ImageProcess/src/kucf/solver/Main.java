package kucf.solver;

import java.io.IOException;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws IOException {
		
		List<TrainingExample> examples = TrainingExampleFinder.getTrainingExamples();
		TrainingExample example = examples.get(0);

		Hypothesis hypothesis = new Hypothesis();
		Output output = hypothesis.process(example.getInput());
		
		System.out.println(example.getInput().getFile());
		System.out.println(example.getTarget());
		System.out.println(output);
		
	}

}
