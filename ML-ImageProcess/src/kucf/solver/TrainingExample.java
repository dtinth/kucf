package kucf.solver;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

public class TrainingExample {
	
	private Output target;
	private Input input;

	public TrainingExample(Input input, Output target) {
		this.input = input;
		this.target = target;
	}
	
	public Input getInput() {
		return input;
	}
	public Output getTarget() {
		return target;
	}

	@Override
	public String toString() {
		return "TrainingExample [target=" + target + ", input=" + input + "]";
	}

}
