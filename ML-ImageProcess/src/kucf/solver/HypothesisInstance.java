package kucf.solver;


public class HypothesisInstance {

	protected Input input;
	public HypothesisInstance(Input input) {
		this.input = input;
	}

	public Output process() {
		Output output = new Output();
		output.setAnswer(0);
		return output;
	}


}
