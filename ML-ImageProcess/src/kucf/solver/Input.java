package kucf.solver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Input {

	private File file;
	public Input(File file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return "Input [file=" + file + "]";
	}

	public BufferedImage getImage() throws IOException {
		return ImageIO.read(file);
	}
	
	public File getFile() {
		return file;
	}

}
