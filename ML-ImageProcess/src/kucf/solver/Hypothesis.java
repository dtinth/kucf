package kucf.solver;

public class Hypothesis {
	
	public Output process(Input input) {
		return new HypothesisInstance(input).process();
	}

}
