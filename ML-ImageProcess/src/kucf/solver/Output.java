package kucf.solver;

public class Output {

	private int answer;
	private int numSegments;
	
	public int getAnswer() {
		return answer;
	}
	public void setAnswer(int answer) {
		this.answer = answer;
	}
	
	public int getNumSegments() {
		return numSegments;
	}
	public void setNumSegments(int numSegments) {
		this.numSegments = numSegments;
	}

	@Override
	public String toString() {
		return "Output [answer=" + answer + "]";
	}
	
}
