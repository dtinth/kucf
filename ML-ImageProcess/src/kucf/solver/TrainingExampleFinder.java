package kucf.solver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TrainingExampleFinder {

	public static List<TrainingExample> getTrainingExamples() {
		List<File> files = FileFinder.findFiles("/Users/dttvb/Downloads/MLdata");
		List<TrainingExample> examples = new ArrayList<>();
		for (File file : files) {
			String name = file.getName();
			if (name.endsWith(".png")) {
				examples.add(createExample(file));
			}
		}
		return examples;
	}

	private static TrainingExample createExample(File file) {
		
		Input input = new Input(file);
		Output target = new Output();
		
		String parentName = file.getParentFile().getName();
		String[] numbers = parentName.split("\\+");

		target.setNumSegments(parentName.length() + 3);
		target.setAnswer(Integer.parseInt(numbers[0]) + Integer.parseInt(numbers[1]));
		
		return new TrainingExample(input, target);
		
	}

}
