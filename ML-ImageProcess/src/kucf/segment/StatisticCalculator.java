package kucf.segment;

import java.util.LinkedList;
import java.util.List;

public class StatisticCalculator {
	
	private int n;
	private List<Double> values = new LinkedList<>();
	private double sum;
	private double mean;
	private double variance;
	private double sd;
	
	public void record(double value) {
		n += 1;
		sum += value;
		values.add(value);
		calculate();
	}

	private void calculate() {
		mean = sum / n;
		for (double value : values) {
			variance += Math.pow(value - mean, 2);
		}
		variance /= n;
		sd = Math.sqrt(variance);
	}

	public void printOut() {
		System.out.printf("n=%d, mean=%f, sd=%f\n", n, mean, sd);
	}

}
