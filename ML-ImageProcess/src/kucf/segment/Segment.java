package kucf.segment;

import java.awt.Point;
import java.util.List;

public class Segment {

	private List<Point> points;
	private int width;
	private int height;

	public Segment(List<Point> points) {
		this.points = points;
		calculateSize();
	}
	
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}

	private void calculateSize() {
		boolean first = true;
		int minX = 0, minY = 0, maxX = 0, maxY = 0;
		for (Point point : points) {
			int x = point.x;
			int y = point.y;
			if (first || x < minX) minX = x;
			if (first || x > maxX) maxX = x;
			if (first || y < minY) minY = y;
			if (first || y > maxY) maxY = y;
			first = false;
		}
		width = maxX - minX;
		height = maxY - minY;
	}
	
	@Override
	public String toString() {
		return String.format("[Segment %dx%d]", width, height);
	}

}
