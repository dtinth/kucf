package kucf.segment;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import kucf.image.Picture;

public class Cutter {

	private BufferedImage image;
	private Picture p;
	private List<Segment> segments;
	private boolean[][] visited;

	private int[][] neighbors = {
			{ 0, -2 }, { 2, 0 }, { 0, 2 }, { -2, 0 },
			{ -1, -1 }, { 0, -1 }, { 1, -1 },
			{ -1,  0 },            { 1,  0 },
			{ -1,  1 }, { 0,  1 }, { 1,  1 }
	};
	private int[][] neighbors2 = {
			{ 0, -3 }, { 0, 3 },
			{ 0, -2 }, { 2, 0 }, { 0, 2 }, { -2, 0 },
			{ -2, -1 }, { -1, -2 }, { 1, -2 }, { 2, -1 },
			{ -2, 1  }, { -1,  2 }, { 1,  2 }, { 2,  1 },
			{ -1, -1 }, { 0, -1 }, { 1, -1 },
			{ -1,  0 },            { 1,  0 },
			{ -1,  1 }, { 0,  1 }, { 1,  1 }
	};

	public Cutter(BufferedImage image) {
		this.image = image;
		this.p = new Picture(image);
	}

	public BufferedImage getImage() {
		return image;
	}
	
	public List<Segment> cut() {
		
		segments = new ArrayList<>();
		visited = new boolean[p.width][p.height];
		
		removeNoise();
		
		for (int y = 0; y < p.height; y ++) {
			for (int x = 0; x < p.width; x ++) {
				if (canGo(x, y)) {
					bfs(x, y);
				}
			}
		}
		
		return segments;
		
	}

	private void removeNoise() {

		for (int y = 0; y < p.height; y ++) {
			for (int x = 0; x < p.width; x ++) {
				boolean alone = true;
				for (int[] neighbor : neighbors) {
					if (canGo(x + neighbor[0], y + neighbor[1])) {
						alone = false;
						break;
					}
				}
				if (alone) {
					p.setRGB(x, y, 255, 255, 255);
				}
			}
		}
		
		
	}

	private boolean canGo(int x, int y) {
		return p.inBound(x, y) && !visited[x][y] && !isWhite(x, y);
	}

	private void bfs(int sx, int sy) {
		
		Queue<Point> fringe = new LinkedList<>();
		List<Point> points = new ArrayList<>();
		
		// Add first point to the fringe.
		fringe.add(new Point(sx, sy));
		if (isBlack(sx, sy)) points.add(new Point(sx, sy));
		visited[sx][sy] = true;
		
		while (!fringe.isEmpty()) {
			Point point = fringe.remove();
			for (int[] neighbor : neighbors2) {
				int x = point.x + neighbor[0];
				int y = point.y + neighbor[1];
				if (canGo(x, y)) {
					fringe.add(new Point(x, y));
					if (isBlack(x, y)) points.add(new Point(x, y));
					visited[x][y] = true;
				}
			}
		}
		
		if (points.size() < 10) {
			return;
		}
		
		int[][] c = {
				{ 255, 0, 0 },
				{ 0, 0, 255 },
				{ 220, 0, 220 },
				{ 0, 128, 255 },
				{ 255, 128, 0 },
				{ 0, 180, 0 }
		};
		
		for (Point point : points) {
			int[] cc = c[segments.size() % c.length];
			p.setRGB(point.x, point.y,
					255 - (int)(255 - (p.r * (cc[0]) / 255.0)),
					255 - (int)(255 - (p.g * (cc[1]) / 255.0)),
					255 - (int)(255 - (p.b * (cc[2]) / 255.0)));
		}
		
		Segment segment = new Segment(points);
		
		if (segment.getWidth() <= 4 && segment.getHeight() <= 5) {
			return;
		}
		
		segments.add(segment);
		
	}

	private boolean isBlack(int x, int y) {
		p.getRGB(x, y);
		return p.r + p.g + p.b < 64;
	}

	private boolean isWhite(int x, int y) {
		p.getRGB(x, y);
		return p.r + p.g + p.b >= 192;
	}

}





