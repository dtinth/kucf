package kucf.segment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import kucf.solver.TrainingExample;
import kucf.solver.TrainingExampleFinder;
import kucf.ui.ImageHelper;

public class SegmentTest {
	
	public static void main(String[] args) throws IOException {
		
		List<TrainingExample> examples = TrainingExampleFinder.getTrainingExamples();
		
		TrainingExample example = examples.get(6389);
		Cutter cutter = new Cutter(example.getInput().getImage());
		ImageHelper.show(example.getInput().getImage());
		cutter.cut();
		ImageHelper.show(cutter.getImage());
		
//
//		
//		StatisticCounter counter = new StatisticCounter("correct", ">", "<");
//		StatisticCalculator calc = new StatisticCalculator();
//		
//		int tolerance = 0;
//		int i = 0;
//		
////		PrintStream s = new PrintStream("/tmp/out.txt");
//		PrintStream s = new PrintStream(new ByteArrayOutputStream());
//
//		for (TrainingExample example : examples) {
//			Cutter cutter = new Cutter(example.getInput().getImage());
//			List<Segment> segments = cutter.cut();
//			int actual = calculateNumberOfSegments(segments);
//			int expected = example.getTarget().getNumSegments();
//			s.printf("%d, %d\n", expected, actual);
//			if (actual < expected - tolerance) {
//				counter.count("<");
//			} else if (actual > expected + tolerance) {
//				counter.count(">");
////				ImageHelper.show(example.getInput().getImage());
////				ImageHelper.show(cutter.getImage());
////				break;
//			} else {
//				counter.count("correct");
////				for (Segment segment : segments) {
////					calc.record(segment.getWidth());
////				}
////				calc.printOut();
//			}
//		}
	}		

	private static int calculateNumberOfSegments(List<Segment> segments) {
		int sum = 0;
		for (Segment segment : segments) {
			sum += segment.getWidth() > 26 ? 2 : 1;
		}
		return sum;
	}

}
