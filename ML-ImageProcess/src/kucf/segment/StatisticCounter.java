package kucf.segment;

import java.util.HashMap;
import java.util.Map;

public class StatisticCounter {
	
	private Map<String, Integer> map = new HashMap<>();
	private String[] names;

	public StatisticCounter(String... names) {
		this.names = names;
		for (String name : names) {
			map.put(name, 0);
		}
	}

	public void count(String name) {
		map.put(name, map.get(name) + 1);
		printOut();
	}

	private void printOut() {
		System.out.println(this);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String name : names) {
			if (sb.length() > 0) sb.append(", ");
			sb.append(name).append("=").append(map.get(name));
		}
		return sb.toString();
	}
	
	

}
