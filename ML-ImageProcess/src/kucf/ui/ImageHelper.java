package kucf.ui;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ImageHelper {

	public static void show(BufferedImage image) {
		JFrame frame = new JFrame("Show picture");
		frame.add(new JLabel(new ImageIcon(image)));
		frame.pack();
		frame.setVisible(true);
	}
	
}
