package kucf.image;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class Picture {

	public BufferedImage image;
	
	public int width;
	public int height;
	public int r, g, b;

	public Picture(BufferedImage image) {
		this.image = image;
		width = image.getWidth();
		height = image.getHeight();
	}
	
	public void getRGB(int x, int y) {
		int rgb = image.getRGB(x, y);
		r = (rgb >> 16) & 0xFF;
		g = (rgb >> 8)  & 0xFF;
		b = (rgb >> 0)  & 0xFF;
	}
	
	public void setRGB(int x, int y, int r, int g, int b) {
		int rgb = new Color(r, g, b).getRGB();
		image.setRGB(x, y, rgb);
	}

	public boolean inBound(int x, int y) {
		return 0 <= x && x < width && 0 <= y && y < height;
	}

}
